import {
    IonContent,
    IonFab,
    IonFabButton,
    IonHeader,
    IonIcon,
    IonPage,
    IonTitle,
    IonToolbar,
    useIonViewWillEnter,
} from '@ionic/react';
import Container from '../components/Container';
import Posts from '../components/Posts';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { postService } from '../redux/posts/actions';
import { add, chatbubbleEllipsesOutline } from 'ionicons/icons';
import AddPostModal from '../components/AddPostModal';
import useUserUid from '../hooks/useUserUid';
import { Space } from 'antd';

const MyPosts = () => {
    const dispatch = useDispatch();
    const { userPosts } = useSelector(state => state.posts);
    const [loading, setLoading] = useState(false);
    const [isModalOpen, setIsModalOpen] = useState(false);

    const { userUid } = useUserUid();

    useIonViewWillEnter(() => {
        setLoading(true);
        dispatch(postService.fetchMyPosts(userUid)).finally(() => setLoading(false));
    }, [userUid]);

    const addPostButtonStyle = { marginBottom: '5%', marginRight: '2%' };

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>
                        <Space>
                            <IonIcon icon={chatbubbleEllipsesOutline} />
                            My posts
                        </Space>
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <IonHeader collapse='condense'>
                    <IonToolbar>
                        <IonTitle size='large'>My posts</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <Container>
                    <Posts
                        posts={userPosts}
                        fetchPosts={offset => {
                            setLoading(true);
                            dispatch(postService.fetchMyPosts(userUid, offset)).finally(() => setLoading(false));
                        }}
                    />
                </Container>
            </IonContent>

            <AddPostModal isOpen={isModalOpen} close={() => setIsModalOpen(false)} />

            <IonFab vertical='bottom' horizontal='end' slot='fixed' style={addPostButtonStyle}>
                <IonFabButton onClick={() => setIsModalOpen(true)}>
                    <IonIcon icon={add} />
                </IonFabButton>
            </IonFab>
        </IonPage>
    );
};

export default MyPosts;
