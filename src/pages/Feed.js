import React, { useEffect, useState } from 'react';
import {
    IonContent,
    IonHeader,
    IonIcon,
    IonLoading,
    IonPage,
    IonTitle,
    IonToolbar,
    useIonViewDidEnter,
} from '@ionic/react';
import Container from '../components/Container';
import Posts from '../components/Posts';
import { useDispatch, useSelector } from 'react-redux';
import { postService } from '../redux/posts/actions';
import { homeOutline } from 'ionicons/icons';
import { Space } from 'antd';

const Feed = () => {
    const dispatch = useDispatch();
    const { posts, lastFetch } = useSelector(state => state.posts);
    const { coords } = useSelector(state => state.geolocation);
    const [isAlreadyFetched, setIsAlreadyFetched] = useState(false);

    // useIonViewDidEnter(() => {
    //     if (!coords) {
    //         return;
    //     }
    //
    //     dispatch(postService.fetchFeed(coords?.lon, coords?.lat));
    // }, [coords]);

    useEffect(() => {
        dispatch(postService.fetchFeed(coords?.lon, coords?.lat)).then(() => setIsAlreadyFetched(true));
    }, [isAlreadyFetched]);

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>
                        <Space>
                            <IonIcon icon={homeOutline} />
                            Feed
                        </Space>
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <IonHeader collapse='condense'>
                    <IonToolbar>
                        <IonTitle size='large'>Feed</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <Container>
                    <Posts
                        posts={posts}
                        fetchPosts={offset => {
                            dispatch(postService.fetchFeed(coords?.lon, coords?.lat, offset));
                        }}
                    />
                </Container>
            </IonContent>
        </IonPage>
    );
};

export default Feed;
