import React, { useState } from 'react';
import {
    IonButton,
    IonContent,
    IonHeader,
    IonIcon,
    IonItem,
    IonLabel,
    IonPage,
    IonText,
    IonTitle,
    IonToolbar,
} from '@ionic/react';
import Container from '../components/Container';
import { alertOutline, settingsOutline } from 'ionicons/icons';
import useUserUid from '../hooks/useUserUid';

import { Popconfirm, Space } from 'antd';
import { useSelector } from 'react-redux';
import { Plugins } from '@capacitor/core';
import moment from 'moment-timezone';

const { Geolocation } = Plugins;

const Settings = () => {
    const { userUid, resetUserUid, uidGenerated } = useUserUid();
    const { coords } = useSelector(state => state.geolocation);

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>
                        <Space>
                            <IonIcon icon={settingsOutline} />
                            Settings
                        </Space>
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <IonHeader collapse='condense'>
                    <IonToolbar>
                        <IonTitle size='large'>Settings</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <Container>
                    <IonItem style={{ fontSize: 12 }}>
                        <IonLabel>User UID:</IonLabel>
                        <IonText slot='end'>
                            <i>{userUid}</i>
                        </IonText>
                    </IonItem>

                    <IonItem>
                        <Popconfirm
                            title='Are you sure？'
                            okText='Yes'
                            cancelText='No'
                            onConfirm={resetUserUid}
                            placement='rightTop'
                        >
                            <IonButton color='danger' slot='end'>
                                <IonIcon icon={alertOutline} slot='end' />
                                Change UID
                            </IonButton>
                        </Popconfirm>
                    </IonItem>

                    <IonItem style={{ fontSize: 12 }}>
                        <IonLabel>Lat:</IonLabel>
                        <IonText slot='end'>
                            <i>{coords.lat}</i>
                        </IonText>
                    </IonItem>

                    <IonItem style={{ fontSize: 12 }}>
                        <IonLabel>Lon:</IonLabel>
                        <IonText slot='end'>
                            <i>{coords.lon}</i>
                        </IonText>
                    </IonItem>

                    <IonItem style={{ fontSize: 12 }}>
                        <IonLabel>Position fetched:</IonLabel>
                        <IonText slot='end'>
                            <i>{moment(coords.timestamp).format('dddd, MMMM Do YYYY, HH:mm:ss')}</i>
                        </IonText>
                    </IonItem>
                </Container>
            </IonContent>
        </IonPage>
    );
};

export default Settings;
