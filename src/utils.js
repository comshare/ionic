import { Plugins } from '@capacitor/core';
import axios from 'axios';


const { Storage } = Plugins;

export const StorageAdapter = {
    get: async key => {
        const { value } = await Storage.get({ key });
        return value;
    },
    set: async (key, value) => {
        return Storage.set({ key, value });
    },
};

export const getDistanceFromLatLonInKm = (lat1, lon1, lat2, lon2) => {
    const deg2rad = deg => deg * (Math.PI / 180);

    const R = 6371; // Radius of the earth in km
    const dLat = deg2rad(lat2 - lat1); // deg2rad below
    const dLon = deg2rad(lon2 - lon1);
    const a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    // Distance in km
    return R * c;
};

export const setAxiosSettings = apiUrl => {
    axios.defaults.baseURL = `${apiUrl}/`;
    axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
}
