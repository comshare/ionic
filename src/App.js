import React, { useEffect, useState } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonIcon, IonLabel, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { homeOutline, chatbubbleEllipsesOutline, settingsOutline } from 'ionicons/icons';

import Feed from './pages/Feed';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

import './App.css';

/* Theme variables */
import './theme/variables.css';
import useGeolocation from './hooks/useGeolocation';
import { setCoords } from './redux/geolocation';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
import MyPosts from './pages/MyPosts';
import { StorageAdapter } from './utils';
import Settings from './pages/Settings';
import { postService } from './redux/posts/actions';
import { locationService } from './redux/locations/actions';
import useUserUid from './hooks/useUserUid';
import { useInterval, useTimeout } from 'ahooks';

const App = () => {
    const dispatch = useDispatch();

    useGeolocation(60, position => {
        console.log('onPositionFetched');
        dispatch(setCoords(position));
    });
    const { coords } = useSelector(state => state.geolocation);

    const { userUid } = useUserUid();
    useInterval(
        () => {
            dispatch(locationService.createLocation(coords.lon, coords.lat, userUid));
        },
        60 * 1000,
        { immediate: true }
    );

    return (
        <IonApp>
            <IonReactRouter>
                <IonTabs>
                    <IonRouterOutlet>
                        <Route path='/feed' component={Feed} exact={true} />
                        <Route path='/my-posts' component={MyPosts} exact={true} />
                        <Route path='/settings' component={Settings} exact={true} />
                        <Route path='/' render={() => <Redirect to='/feed' />} exact={true} />
                    </IonRouterOutlet>

                    <IonTabBar slot='bottom'>
                        <IonTabButton tab='feed' href='/feed'>
                            <IonIcon icon={homeOutline} />
                            <IonLabel>Feed</IonLabel>
                        </IonTabButton>

                        <IonTabButton tab='my-posts' href='/my-posts'>
                            <IonIcon icon={chatbubbleEllipsesOutline} />
                            <IonLabel>My posts</IonLabel>
                        </IonTabButton>

                        <IonTabButton tab='settings' href='/settings'>
                            <IonIcon icon={settingsOutline} />
                            <IonLabel>Settings</IonLabel>
                        </IonTabButton>
                    </IonTabBar>
                </IonTabs>
            </IonReactRouter>
        </IonApp>
    );
};

export default App;
