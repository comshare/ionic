import { IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonIcon } from '@ionic/react';
import React from 'react';
import moment from 'moment-timezone';
import { Tag } from 'antd';

const Comment = ({ comment }) => {
    const commentContent = comment?.content;
    const commentCreated = moment(comment?.created).format('dddd, MMMM Do YYYY, HH:mm:ss');
    const userHandle = comment?.user_handle;

    return (
        <IonCard>
            <IonCardHeader>
                <IonCardSubtitle>
                    <span style={{ fontSize: 10 }}>
                        By <i>{userHandle}</i> on {commentCreated}
                    </span>
                </IonCardSubtitle>
            </IonCardHeader>

            <IonCardContent>{commentContent}</IonCardContent>
        </IonCard>
    );
};

export default Comment;
