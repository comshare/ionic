import {
    IonBackButton,
    IonButton,
    IonButtons,
    IonContent,
    IonHeader,
    IonIcon,
    IonModal,
    IonText,
    IonTextarea,
    IonTitle,
    IonToolbar,
} from '@ionic/react';
import React, { useState } from 'react';
import { Button, Form, Input, Row, message, Space } from 'antd';
import { postService } from '../redux/posts/actions';
import { useDispatch, useSelector } from 'react-redux';
import useUserUid from '../hooks/useUserUid';
import { chatbubbleEllipsesOutline, createOutline } from 'ionicons/icons';

const AddPostModal = ({ isOpen, close }) => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);

    const { userUid } = useUserUid();
    const { coords } = useSelector(state => state.geolocation);

    const onCreatePost = values => {
        if (!coords?.lat || !coords?.lon) {
            message.error('Cannot determine user location');
        }
        setLoading(true);
        dispatch(postService.createPost(values, coords.lon, coords.lat, userUid))
            .then(close)
            .finally(() => setLoading(false));
    };

    return (
        <IonModal isOpen={isOpen} onDidDismiss={close}>
            <IonContent>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot='start'>
                            <IonBackButton defaultHref='/my-posts' onClick={close} />
                        </IonButtons>
                        <IonTitle>
                            <Space>
                                <IonIcon icon={createOutline} />
                                Create a post
                            </Space>
                        </IonTitle>
                    </IonToolbar>
                </IonHeader>

                <Row align='center'>
                    <Form layout='vertical' name='basic' onFinish={onCreatePost} style={{ width: '80%' }}>
                        <Form.Item
                            label={<IonText color='primary'>Title</IonText>}
                            name='title'
                            rules={[{ required: true, message: 'Please enter title!' }]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label={<IonText color='primary'>Message</IonText>}
                            name='content'
                            rules={[{ required: true, message: 'Please enter a message!' }]}
                        >
                            <Input.TextArea autoSize={{ minRows: 4, maxRows: 6 }} />
                        </Form.Item>

                        <Form.Item>
                            <Button type='primary' htmlType='submit'>
                                Post
                            </Button>
                        </Form.Item>
                    </Form>
                </Row>
            </IonContent>
        </IonModal>
    );
};

export default AddPostModal;
