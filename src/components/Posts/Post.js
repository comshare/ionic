import { IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonIcon } from '@ionic/react';
import React from 'react';
import moment from 'moment-timezone';
import { Col, Divider, Row, Space } from 'antd';
import { chatbubblesOutline, heartOutline } from 'ionicons/icons';
import { getDistanceFromLatLonInKm } from '../../utils';
import { useSelector } from 'react-redux';

const Post = ({ post, onCommentsClicked = post => null }) => {
    const { coords } = useSelector(state => state.geolocation);

    const postTitle = post?.title;
    const postContent = post?.content;
    const postCreated = moment(post?.created).format('dddd, MMMM Do YYYY, HH:mm:ss');
    const commentsCount = post?.comments_count;
    const userHandle = post?.user_handle;

    const { longitude, latitude } = post.location;
    const distance = Math.round(getDistanceFromLatLonInKm(longitude, latitude, coords.lat, coords.lon));

    return (
        <IonCard>
            <IonCardHeader>
                <IonCardSubtitle>
                    <Row>
                        <Col span={18}>
                            <span style={{ fontSize: 10 }}>
                                {postCreated} by <i>{userHandle}</i>
                                <br />
                                {distance} km from you
                            </span>
                        </Col>
                        <Col span={6}>
                            <Row justify='end'>
                                <Space>
                                    <Space>
                                        <a>
                                            <IonIcon icon={heartOutline} /> 0
                                        </a>
                                    </Space>
                                    <Space>
                                        <a onClick={() => onCommentsClicked(post)}>
                                            <IonIcon icon={chatbubblesOutline} /> {commentsCount}
                                        </a>
                                    </Space>
                                </Space>
                            </Row>
                        </Col>
                    </Row>
                </IonCardSubtitle>
                <IonCardTitle>{postTitle}</IonCardTitle>
            </IonCardHeader>

            <IonCardContent>{postContent}</IonCardContent>
        </IonCard>
    );
};

export default Post;
