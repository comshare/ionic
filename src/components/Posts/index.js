import React, { useEffect, useState } from 'react';
import Post from './Post';
import { IonButton, IonCard, IonRefresher, IonRefresherContent } from '@ionic/react';
import PostDetailModal from '../PostDetailModal';
import { Empty, message } from 'antd';
import { chevronDownCircleOutline } from 'ionicons/icons';

const Posts = ({ posts, fetchPosts }) => {
    const [offset, setOffset] = useState(0);
    const [selectedPost, setSelectedPost] = useState(null);
    const renderPosts = () =>
        posts.map((post, index) => <Post key={index} post={post} onCommentsClicked={post => setSelectedPost(post)} />);

    useEffect(() => {
        console.log('fetching posts');
        fetchPosts && fetchPosts(offset);
    }, [offset]);

    const onRefresh = async event => {
        console.log('onRefresh!');
        fetchPosts && (await fetchPosts());
        event.detail.complete();
        message.info('Posts has been updated.');
    };

    const loadMore = () => setOffset(offset + 10);

    // if (posts.length === 0) {
    //     return <Empty style={{ marginTop: 50 }} description='There is no posts' />;
    // }

    return (
        <div>
            <IonRefresher slot='fixed' onIonRefresh={onRefresh}>
                <IonRefresherContent
                    pullingIcon={chevronDownCircleOutline}
                    pullingText='Pull to refresh'
                    refreshingSpinner='circles'
                    refreshingText='Refreshing...'
                />
            </IonRefresher>
            {renderPosts()}

            <IonCard hidden={posts.length < offset + 10}>
                <IonButton style={{ width: '100%' }} onClick={loadMore}>
                    Load more
                </IonButton>
            </IonCard>

            <PostDetailModal post={selectedPost} close={() => setSelectedPost(null)} />
        </div>
    );
};

export default Posts;
