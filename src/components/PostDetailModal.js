import {
    IonBackButton,
    IonButton,
    IonButtons,
    IonContent,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonList,
    IonModal,
    IonTitle,
    IonToolbar,
} from '@ionic/react';
import React, { useEffect, useState } from 'react';
import useUserUid from '../hooks/useUserUid';
import Post from './Posts/Post';
import axios from 'axios';
import { send, sendOutline } from 'ionicons/icons';
import { incrementPostCommentsCount } from '../redux/posts/postsSlice';
import { useDispatch } from 'react-redux';
import Comments from './Comments';
import { message } from 'antd';

const PostDetailModal = ({ close, post }) => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);
    const [comments, setComments] = useState([]);
    const [content, setContent] = useState(null);

    const { userUid } = useUserUid();

    const createComment = () => {
        setContent(null);
        const payload = {
            content,
            post_id: post.id,
        };
        axios.post('comments/', payload, { headers: { 'user-uid': userUid } }).then(response => {
            setComments([...comments, response.data].filter(comment => comment.created));
            dispatch(incrementPostCommentsCount({ id: post.id }));
            message.success('Comment published.');
        });
    };

    useEffect(() => {
        if (post) {
            setLoading(true);
            axios
                .get(`posts/${post.id}/comments/`)
                .then(({ data }) => setComments(data))
                .finally(() => setLoading(false));
        }
    }, [post]);

    return (
        <IonModal isOpen={!!post} onDidDismiss={close}>
            <IonContent>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot='start'>
                            <IonBackButton defaultHref={null} onClick={close} />
                        </IonButtons>
                        <IonTitle>Post details</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <Post post={post} />
                <Comments comments={comments} />

                <IonItem>
                    <IonButton color='danger' slot='end' onClick={createComment} hidden={!content}>
                        <IonIcon icon={send} />
                    </IonButton>
                    <IonInput
                        value={content}
                        placeholder='Write a comment...'
                        onIonChange={e => setContent(e?.detail?.value)}
                    />
                </IonItem>
            </IonContent>
        </IonModal>
    );
};

export default PostDetailModal;
