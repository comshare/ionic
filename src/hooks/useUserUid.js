import { useState } from 'react';
import { StorageAdapter } from '../utils';
import { v4 as uuidv4 } from 'uuid';

const useUserUid = () => {
    const key = 'userUid';
    const [userUid, setUserUid] = useState(null);
    const [uidGenerated, setUidGenerated] = useState(null);

    StorageAdapter.get('userUid').then(value => {
        if (!value) {
            resetUserUid().then(newUserUid => setUserUid(newUserUid));
        } else {
            setUserUid(value);
        }
    });
    StorageAdapter.get('uidGenerated').then(value => setUidGenerated(value));

    const resetUserUid = async () => {
        const newUserUid = uuidv4();
        await StorageAdapter.set(key, newUserUid);
        setUserUid(newUserUid);
        return newUserUid;
    };

    return { userUid, resetUserUid, uidGenerated };
};

export default useUserUid;
