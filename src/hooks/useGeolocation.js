import { Plugins } from '@capacitor/core';

import { useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

const { Geolocation } = Plugins;

const useGeolocation = (intervalInSeconds = null, onPositionFetched = null) => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState({ showError: false });
    const [position, setPosition] = useState();

    const getLocation = useCallback(() => {
        console.log('Fetching new location...');
        setLoading(true);

        Geolocation.getCurrentPosition()
            .then(position => {
                const value = {
                    lat: position.coords.latitude,
                    lon: position.coords.longitude,
                    timestamp: position.timestamp,
                };
                console.log('position fetched', value);
                setPosition(value);
                setError({ showError: false });
                onPositionFetched && onPositionFetched(value);
            })
            .catch(e => setError({ showError: true, message: e.message }))
            .finally(() => setLoading(false));
    }, [dispatch]);

    useEffect(() => {
        getLocation();
        if (!intervalInSeconds) {
            return;
        }

        const interval = setInterval(() => {
            getLocation();
        }, intervalInSeconds * 1000);
        return () => clearInterval(interval);
    }, [intervalInSeconds, getLocation]);

    return { position, error, loading, getLocation };
};

export default useGeolocation;
