import { combineReducers } from 'redux';
import postsReducer from './posts/postsSlice';
import geolocationReducer from './geolocation';

const rootReducer = combineReducers({
    posts: postsReducer,
    geolocation: geolocationReducer,
});

export default rootReducer;
