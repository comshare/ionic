import axios from 'axios';
import { addUserPost, extendPosts, extendUserPosts, setPosts, setUserPosts } from './postsSlice';

export const postService = {
    fetchFeed: (lon, lat, offset, limit) => dispatch => {
        if (!lon || !lat) {
            return Promise.reject();
        }

        const params = { lon, lat, offset };
        return axios.get('feed/', { params }).then(response => {
            if (offset) {
                return dispatch(extendPosts(response.data));
            }
            return dispatch(setPosts(response.data));
        });
    },

    fetchMyPosts: (userUid, offset) => dispatch => {
        const config = { headers: { 'user-uid': userUid }, params: { offset } };
        return axios.get('user/posts/', config).then(response => {
            if (offset) {
                dispatch(extendUserPosts(response.data));
            } else {
                dispatch(setUserPosts(response.data));
            }
        });
    },

    createPost: (body, lon, lat, userUid) => dispatch => {
        const payload = {
            ...body,
            location: {
                latitude: lat,
                longitude: lon,
            },
        };
        return axios
            .post('posts/', payload, { headers: { 'user-uid': userUid } })
            .then(({ data }) => dispatch(addUserPost(data)));
    },
};
