import { createSlice } from '@reduxjs/toolkit';
import moment from 'moment-timezone';

const initialState = {
    posts: [],
    userPosts: [],
    lastFetched: null,
};

const postsSlice = createSlice({
    name: 'posts',
    initialState: initialState,
    reducers: {
        setPosts: (state, action) => {
            state.posts = action.payload;
            state.lastFetched = moment.now();
        },
        extendPosts: (state, action) => {
            state.posts.push(...action.payload);
        },
        updateLastFetched: state => {
            state.lastFetched = moment.now();
        },
        addPost: (state, action) => {
            state.posts.push(action.payload);
        },

        incrementPostCommentsCount: (state, action) => {
            const post = state.posts.find(post => post.id === action.payload.id);
            if (post) {
                post.comments_count = post.comments_count + 1;
            }
        },

        setUserPosts: (state, action) => {
            state.userPosts = action.payload;
        },
        addUserPost: (state, action) => {
            state.userPosts.unshift(action.payload);
        },
        extendUserPosts: (state, action) => {
            state.userPosts.push(...action.payload);
        },
    },
});

const postsReducer = postsSlice.reducer;
export default postsReducer;

export const {
    setPosts,
    updateLastFetched,
    addPost,
    setUserPosts,
    addUserPost,
    incrementPostCommentsCount,
    extendPosts,
    extendUserPosts,
} = postsSlice.actions;
