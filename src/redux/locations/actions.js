import axios from 'axios';

export const locationService = {
    createLocation: (lon, lat, userUid) => dispatch => {
        console.log('createLocation', lon, lat, userUid);
        if (!lon || !lat || !userUid) {
            console.log('Not sending location as info is incomplete')
            return;
        }
        const payload = {
            location: {
                latitude: lat,
                longitude: lon,
            },
        };
        return axios.post('locations/', payload, { headers: { 'user-uid': userUid } });
    },
};
