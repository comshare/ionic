import { createSlice } from '@reduxjs/toolkit';
import moment from 'moment-timezone';

const initialState = {
    coords: {},
    lastFetched: null,
};

const geolocationSlice = createSlice({
    name: 'geolocation',
    initialState: initialState,
    reducers: {
        setCoords: (state, action) => {
            state.coords = action.payload;
            state.lastFetched = moment.now();
        },
    },
});

const geolocationReducer = geolocationSlice.reducer;
export default geolocationReducer;

export const { setCoords } = geolocationSlice.actions;
